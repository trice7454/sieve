//
//  main.c
//  sieve
//
//  Created by Terry Rice on 2/25/13.
//  Copyright (c) 2013 Terry Rice. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

#define MAX_NUMBER 1000000

struct numberType {
    int number;
    char flagged;
};

typedef struct numberType SieveNumber;

int main(int argc, const char * argv[])
{
    int p, i, refNum;
    
    SieveNumber *numbers = (SieveNumber *)malloc(sizeof(SieveNumber) * (MAX_NUMBER+1));
    
    p=2;
    for (i=0; i<MAX_NUMBER+1; i++,p++) {
        numbers[i].number = p;
        numbers[i].flagged = 0;
    }
    
    refNum = 2;
    p = refNum;
    i=0;
    p+=refNum;
    while (1) {
        if (i <= MAX_NUMBER) {
            if (numbers[i].number == p && refNum != numbers[i].number) {
                numbers[i].flagged += 1;
                p+=refNum;
            }
            if (p > numbers[MAX_NUMBER].number) {
                ++refNum;
                p = refNum;
                p += refNum;
                i=0;
                if (p >= numbers[MAX_NUMBER].number) {
                    break;
                }                
            }
        }
        i++;
    }
    
    printf("Primes:\n");
    for (i=0; i<=MAX_NUMBER; i++) {
        if (numbers[i].flagged == 0) {
            printf("%d\n", numbers[i].number);            
        }
    }
    
    free(numbers);
    return 0;
}

